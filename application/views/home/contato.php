<div class="container">
<br/>
<br/>
<br/>
<br/>

<!--Section: Contact v.2-->
<section class="mb-4">

    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4">Envie-nos uma mensagem</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5">Você tem alguma pergunta? Por favor, não hesite em nos contatar diretamente. Nossa equipe entrará em contato com você em questão de horas para ajudá-lo.</p>

    <div class="row">

        <!--Grid column-->
        <div class="col-md-9 mb-md-0 mb-5">
            <form id="form_contato" name="form_contato"  method="POST">

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="nome" name="nome" class="form-control">
                            <label for="nome" class="">Seu nome</label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="">Seu email</label>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="assunto" name="assunto" class="form-control">
                            <label for="assunto" class="">Assunto</label>
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="mensagem" name="mensagem" rows="2" class="form-control md-textarea"></textarea>
                            <label for="mensagem">Sua mensagem</label>
                        </div>

                    </div>
                </div>
                <!--Grid row-->

                <div class="text-center text-md-left black-color">
                    <input type="submit" class="btn btn-black" id="enviar"  name="enviarBTN" value="ENVIAR"/>
                </div>
                <div class="status"></div>

            </form>

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fas fa-map-marker-alt fa-2x"></i>
                    <p>Av. Salgado Filho n° 3501, Vila Rio , Brasil - SP - Guarulhos</p>
                </li>

                <li><i class="fas fa-phone mt-4 fa-2x"></i>
                    <p>+55 011 2345-6789</p>
                </li>

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <p>contato@infotech.com.br</p>
                </li>
            </ul>
        </div>
        <!--Grid column-->

    </div>

</section>
<!--Section: Contact v.2-->

</div>