<br/>
<br/>
<br/>

<div class="container">
<!--Main layout-->
<main>

  <!--Navbar-->
  <nav class="navbar navbar-expand-lg navbar-dark mdb-color lighten-3 mt-3 mb-5">

    <!-- Navbar brand -->
    <span class="navbar-brand">Categorias:</span>

    <!-- Collapse button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
      aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="basicExampleNav">
    
      <!-- Links -->
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?= base_url("HomePage/servicos")?>">Todas as ofertas
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url("HomePage/servicos")?>">Notebooks e 2 em 1</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url("HomePage/servicos")?>">Desktops e All in Ones</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url("HomePage/servicos")?>">Notebooks Gamer e Acessórios</a>
        </li>

      </ul>
      <!-- Links -->

      <form class="form-inline">
        <div class="md-form my-0">
          <input class="form-control mr-sm-2" type="text" placeholder="Procurar" aria-label="Search">
        </div>
      </form>
    </div>
    <!-- Collapsible content -->

  </nav>
  <!--/.Navbar-->

  <!--Section: Products v.3-->
  <section class="text-center mb-4">

    <!--Grid row-->
    <div class="row wow fadeIn">

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4">

        <!--Card-->
        <div class="card">

          <!--Card image-->
          <div class="view overlay">
          <a href="<?= base_url("HomePage/servicos")?>">
            <img src="<?php echo base_url('images/home/home05.jpg'); ?>" class="card-img-top"
              alt="">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
          <!--Card image-->

          <!--Card content-->
          <div class="card-body text-center">
            <!--Category & Title-->
            <a href="<?= base_url("HomePage/servicos")?>" class="dark-grey-text">
            <h5>
            <strong>Pc Gamer CompSmart Pc SMT81263 </strong>
            <h5>
            </a>
            <h5>
                <a href="<?= base_url("HomePage/detalhes")?>" class="grey-text">Intel i5 8GB (GeForce GTX 1650 4GB) 1TB + Cadeira Gamer
                <span class="badge badge-pill danger-color">Lançamento</span>
                </a>
            </h5>


            <h4 class="font-weight-bold blue-text">
              <strong>R$ 3.407,04</strong>
            </h4>

          </div>
          <!--Card content-->

        </div>
        <!--Card-->

      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4">

        <!--Card-->
        <div class="card">

          <!--Card image-->
          <div class="view overlay">
          <a href="<?= base_url("HomePage/servicos")?>">
            <img src="<?php echo base_url('images/home/home01.jpg'); ?>" class="card-img-top"
              alt="">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
          <!--Card image-->

          <!--Card content-->
          <div class="card-body text-center">
            <!--Category & Title-->
            <a href="<?= base_url("HomePage/servicos")?>">
            <h5>
            <strong>Computador Completo EasyPC </strong>
            <h5>
            </a>
            <h5>
            <a href="<?= base_url("HomePage/detalhes")?>" class="grey-text">Standard Intel Core i5 8GB Hd 3TB Monitor 19.5” HDMI LED
                <span class="badge badge-pill primary-color">Mais vendido</span>
                </a>
            </h5>

            <h4 class="font-weight-bold blue-text">
              <strong>R$ 1.599,00</strong>
            </h4>

          </div>
          <!--Card content-->

        </div>
        <!--Card-->

      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4">

        <!--Card-->
        <div class="card">

          <!--Card image-->
          <div class="view overlay">
          <a href="<?= base_url("HomePage/servicos")?>">
            <img src="<?php echo base_url('images/home/home02.jpg'); ?>" class="card-img-top"
              alt="">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
          <!--Card image-->

          <!--Card content-->
          <div class="card-body text-center">
            <!--Category & Title-->
            <a href="<?= base_url("HomePage/servicos")?>" class="dark-grey-text">
            <h5>
            <strong> Computador ICC IV2583CM19</strong>
            <h5>
            </a>
            <h5>
            <a href="<?= base_url("HomePage/detalhes")?>" class="grey-text">Intel Core I5 3.20 ghz 8GB HD 2TB DVDRW Kit Multimídia Monitor LED 19,5” HDMI FULLHD
                </a>
            </h5>

            <h4 class="font-weight-bold blue-text">
              <strong>R$ 1.449,00</strong>
            </h4>

          </div>
          <!--Card content-->

        </div>
        <!--Card-->

      </div>
      <!--Grid column-->

      <!--Fourth column-->
      <div class="col-lg-3 col-md-6 mb-4">

        <!--Card-->
        <div class="card">

          <!--Card image-->
          <div class="view overlay">
          <a href="<?= base_url("HomePage/servicos")?>">
            <img src="<?php echo base_url('images/home/home07.jpg'); ?>" class="card-img-top"
              alt="">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
          <!--Card image-->

          <!--Card content-->
          <div class="card-body text-center">
            <!--Category & Title-->
            <a href="<?= base_url("HomePage/servicos")?>" class="dark-grey-text">
            <h5>
            <strong>Computador Completo CorpC</strong>
            <h5>
            </a>
            <h5>
            <a href="<?= base_url("HomePage/detalhes")?>" class="grey-text">com Monitor LED HDMI 19.5” Intel Core i5 4GB HD 500GB Memória 4GB
                <span class="badge badge-pill danger-color">Novo</span>

                </a>
            </h5>

            <h4 class="font-weight-bold blue-text">
              <strong>R$ 1.299,00</strong>
            </h4>

          </div>
          <!--Card content-->

        </div>
        <!--Card-->

      </div>
      <!--Fourth column-->

    </div>
    <!--Grid row-->

    <!--Grid row-->
    <div class="row wow fadeIn">

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4">

        <!--Card-->
        <div class="card">

          <!--Card image-->
          <div class="view overlay">
          <a href="<?= base_url("HomePage/servicos")?>">
            <img src="<?php echo base_url('images/home/home04.jpg'); ?>" class="card-img-top"
              alt="">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
          <!--Card image-->

          <!--Card content-->
          <div class="card-body text-center">
            <!--Category & Title-->
            <a href="<?= base_url("HomePage/servicos")?>" class="dark-grey-text">
            <h5>
            <strong>Computador Gamer Completo </strong>
            <h5>
            </a>
            <h5>
            <a href="<?= base_url("HomePage/detalhes")?>" class="grey-text">com Monitor LED Intel Core i5 8GB HD 500GB (Nvidia Geforce GT) Kit gamer com mousepad EasyPC Light
                </a>
            </h5>

            <h4 class="font-weight-bold blue-text">
              <strong>R$1.890,00</strong>
            </h4>

          </div>
          <!--Card content-->

        </div>
        <!--Card-->

      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4">

        <!--Card-->
        <div class="card">

          <!--Card image-->
          <div class="view overlay">
          <a href="<?= base_url("HomePage/servicos")?>">
            <img src="<?php echo base_url('images/home/home06.jpg'); ?>" class="card-img-top"
              alt="">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
          <!--Card image-->

          <!--Card content-->
          <div class="card-body text-center">
            <!--Category & Title-->
            <a href="<?= base_url("HomePage/servicos")?>" class="dark-grey-text">
            <h5>
            <strong>Computador Desktop Completo</strong>
            <h5>
            </a>
            <h5>
            <a href="<?= base_url("HomePage/detalhes")?>" class="grey-text">com Monitor LED HDMI Intel Core i5 8GB HD 2TB com caixas de som mouse e teclado EasyPC Standard Plus
              </a>
            </h5>

            <h4 class="font-weight-bold blue-text">
              <strong>R$ 1.499,00</strong>
            </h4>

          </div>
          <!--Card content-->

        </div>
        <!--Card-->

      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4">

        <!--Card-->
        <div class="card">

          <!--Card image-->
          <div class="view overlay">
          <a href="<?= base_url("HomePage/servicos")?>">
            <img src="<?php echo base_url('images/home/home08.jpg'); ?>" class="card-img-top"
              alt="">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
          <!--Card image-->

          <!--Card content-->
          <div class="card-body text-center">
            <!--Category & Title-->
            <a href="<?= base_url("HomePage/servicos")?> class="dark-grey-text">
            <h5>
            <strong>Computador Completo Easy PC </strong>
            <h5>
            </a>
            <h5>
            <a href="<?= base_url("HomePage/detalhes")?>" class="grey-text">Connect Intel Core i5 (Gráficos Intel HD) 8GB HD 2TB Monitor 19.5 LED HDMI Memória 8GB
                <span class="badge badge-pill primary-color">Mais vendido</span>              
                </a>
            </h5>

            <h4 class="font-weight-bold blue-text">
              <strong>R$ 1.599,00</strong>
            </h4>

          </div>
          <!--Card content-->

        </div>
        <!--Card-->

      </div>
      <!--Grid column-->

      <!--Fourth column-->
      <div class="col-lg-3 col-md-6 mb-4">

        <!--Card-->
        <div class="card">

          <!--Card image-->
          <div class="view overlay">
          <a href="<?= base_url("HomePage/servicos")?>">
          <img src="<?php echo base_url('images/home/home03.jpg'); ?>" class="card-img-top"
              alt="">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
          <!--Card image-->

          <!--Card content-->
          <div class="card-body text-center">
            <!--Category & Title-->
            <a href="<?= base_url("HomePage/servicos")?>" class="dark-grey-text">
            <h5>
            <strong>Desktop Empresarial OptiPlex 3060 </strong>
            <h5>
            </a>
            <h5>
                <a href="<?= base_url("HomePage/detalhes")?>" class="grey-text">Micro-P20M 8ª Geração Intel Core i3 4GB 500GB Win 10 Pro Monitor - Dell
                <span class="badge badge-pill danger-color">Promoção</span>
                </a>
            </h5>

            <h4 class="font-weight-bold blue-text">
              <strong>R$ 2.798,00</strong>
            </h4>

          </div>
          <!--Card content-->

        </div>
        <!--Card-->

      </div>
      <!--Fourth column-->

    </div>
    <!--Grid row-->

  </section>
  <!--Section: Products v.3-->



</main>
<br/>
<br/>
<!--Main layout-->
</div>
