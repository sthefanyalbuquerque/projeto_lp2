
<!--Main Navigation-->

  <nav class="navbar fixed-top navbar-expand-lg navbar-dark pink scrolling-navbar">
    <a class="navbar-brand" href="<?= base_url("HomePage/index")?>"><strong>INFOTech</strong></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url("HomePage/servicos")?>">Serviços</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url("HomePage/sobre")?>">A Empresa</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url("HomePage/contato")?>">Contato</a>
        </li>
      </ul>
      <ul class="navbar-nav nav-flex-icons">
        <li class="nav-item">
            <a href="<?= base_url("HomePage/itens_compras")?>" class="nav-link waves-effect">
              <span class="badge red z-depth-1 mr-1"> 0 </span>
              <i class="fas fa-shopping-cart"></i>
              <span class="clearfix d-none d-sm-inline-block"> Carrinho </span>
            </a>
        </li>
        <li class="nav-item">
          <a href="<?= base_url("HomePage/login")?>" class="nav-link"><i class="fas fa-sign-in-alt"></i>
          <span class="clearfix d-none d-sm-inline-block"> Login </span>
          </a>
        </li>
        <!-- <li class="nav-item">
        <a class="nav-link"><i class="fas fa-sign-out-alt"></i>
          <span class="clearfix d-none d-sm-inline-block"> Cadastre-se </span>
          </a>
        </li>-->
      </ul>
    </div>
  </nav>

<!--Main Navigation-->
