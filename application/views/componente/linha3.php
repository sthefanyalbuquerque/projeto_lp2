        <!--Grid row-->
        <div class="row wow fadeIn">

          <!--Grid column-->
          <div class="col-lg-5 col-xl-4 mb-4">
            <!--Featured image-->
            <div class="view overlay rounded z-depth-1">
              <img src="https://mdbootstrap.com/wp-content/uploads/2018/01/push-fb.jpg" class="img-fluid" alt="">
              <a href="https://mdbootstrap.com/education/tech-marketing/web-push-introduction/" target="_blank">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
            <h3 class="mb-3 font-weight-bold dark-grey-text">
              <strong>Web Push notifications</strong>
            </h3>
            <p class="grey-text">Push messaging provides a simple and effective way to re-engage with your users and in
              this tutorial
              you'll learn how to add push notifications to your web app</p>
            <a href="https://mdbootstrap.com/education/tech-marketing/web-push-introduction/" target="_blank" class="btn btn-primary btn-md">Start
              tutorial
              <i class="fas fa-play ml-2"></i>
            </a>
          </div>
          <!--Grid column-->

        </div>
        <!--Grid row-->

        <hr class="mb-5">
