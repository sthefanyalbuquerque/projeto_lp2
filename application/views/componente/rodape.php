
<!-- Footer -->
<footer class="page-footer font-small footer-dark pink pt-4">

  <!-- Footer Links -->
  <div class="container text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-4 mx-auto">

        <!-- Content -->
        <h5 class="font-weight mt-3 mb-4">© 2018 INFOTech Inc. Todos os direitos reservados.</h5>
        <p>CNPJ 37.184.985/0002-23 <br/>
           Empresa beneficiada pela Lei de Informática. <br/>
          Av. Industrial Belgraf, 400 <br/>
          Bairro Medianeira <br/>
          Eldorado do Sul - RS <br/>
          CEP 92990-000 <br/>
         <a href="<?= base_url("HomePage/index")?>">INFOTech</a></p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none">

      <!-- Grid column -->
      <div class="col-md-6 ">

        <!-- Links -->
        <ul class="list-unstyled">
          <li>
            <p>Garantia total (legal + contratual) de 01 ano, inclui peças e mão de obra, restrita aos produtos fornecidos pela INFOTech. Na garantia no centro de reparos, o Cliente, após contato telefônico com o Suporte Técnico da INFOTech com diagnóstico remoto, deverá levar o seu equipamento 
              ao centro de reparos localizado em SP ou encaminhar pelos Correios, esse último sem ônus, desde que seja preservada a caixa original do produto. Na garantia à domicílio/assistência técnica no local, técnicos serão deslocados, se necessário, após consulta telefônica 
              com diagnóstico remoto. Produtos e softwares de outras marcas estão sujeitos aos termos de garantia dos respectivos fabricantes, conforme o respectivo site. Para mais detalhes sobre a garantia do seu equipamento, consulte o seu representante de vendas ou visite o site 
              <a href="<?= base_url("HomePage/index")?>">INFOTech</a></p>
          </li>
          <li>
            <a href="http://www.planalto.gov.br/ccivil_03/Leis/L8078.htm" target="_blank">Para consultar o Código de Defesa do Consumido</a>
          </li>
        </ul>

      </div>
      <br/>
      <!-- Grid column 

       <hr class="clearfix w-100 d-md-none">-->

    </div>
    <!-- Grid row -->

    <hr class="clearfix w-100 d-md-none">
  </div>
  <!-- Footer Links -->

  

  <!-- Social buttons -->
  <ul class="list-unstyled list-inline text-center">
    <li class="list-inline-item">
      <a href="https://www.facebook.com" target="_blank" class="btn-floating btn-fb mx-1">
        <i class="fab fa-facebook-f"> </i>
      </a>
    </li>
    <li class="list-inline-item">
      <a href="https://twitter.com" target="_blank" class="btn-floating btn-tw mx-1">
        <i class="fab fa-twitter"> </i>
      </a>
    </li>
    <li class="list-inline-item">
      <a href="https://www.linkedin.com/" target="_blank" class="btn-floating btn-gplus mx-1">
      <i class="  fab fa-linkedin-in"> </i>
      </a>
    </li>
    <li class="list-inline-item">
      <a href="https://imsthefanny@bitbucket.org/imsthefanny/projeto_lp2" target="_blank" class="btn-floating btn-li mx-1">
        <i class="fab fa-bitbucket"></i>
      </a>
    </li>
    <li class="list-inline-item">
      <a href="https://github.com/s-albuquerque" target="_blank" class="btn-floating btn-dribbble mx-1">
        <i class="fab fa-github"> </i>
      </a>
    </li>
  </ul>
  <!-- Social buttons -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2018 Copyright:
    <p> IFSP CAMPUS GUARULHOS</p>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->