<main class="mt-5 pt-5">
    <div class="container">
      <section class="card blue-gradient wow fadeIn" id="intro">
        <div class="card-body text-white text-center py-5 px-5 my-5">
        
          <h1 class="mb-4"><strong><?= $titulo ?></strong></h1>
          <p><strong><?= $subtitulo ?></strong></p>
          <p class="mb-4"><?= $descricao ?></p>
          <a target="_blank" href="https://mdbootstrap.com/education/bootstrap/" class="btn btn-outline-white btn-lg">
            <?= $rotulo_botao ?>
            <i class="fas fa-graduation-cap ml-2"></i>
          </a>

        </div>
      </section>
