<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $titulo ?></title>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/mdb.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/style.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/painel.css') ?>" rel="stylesheet">
</head>
<body>

    <br/>
    <br/>
    <div class="row md-5">
        <div class="col col3">&nbsp;</div>
        <div class="col col6">
            <h2 class="text-center"><?php echo $h2 ?></h2><br/>
            <?php 
                echo form_open();
                echo form_label('User de login:', 'login');
                echo form_input('login', set_value('login'), array('autofocus' => 'autofocus'));
                echo form_label('Email do administrador:', 'email');
                echo form_input('email', set_value('email'));
                echo form_label('Senha:', 'senha');
                echo form_password('senha', set_value('senha'));
                echo form_label('Repita a senha:', 'senha2');
                echo form_password('senha2', set_value('senha2'));
                echo form_submit('envia', 'Salvar dados', array('class' => 'botao'));
                echo form_close();

            ?>

       </div>
       <div class="col col3">&nbsp;</div>
    </div>

</body>
</html>