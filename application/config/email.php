<?php
/*
 * Arquivo Configuração de Email
 */


$config['protocol']  = 'smtp'; // Podendo ser alterado para mail caso você queira enviar com o mail do php.
$config['charset'] = 'iso-8859-1';
$config['wordwrap'] = TRUE;
$config['smtp_host'] = '';
$config['smtp_user'] = '';
$config['smtp_pass'] = '';
$config['smtp_timeout'] = 20;
$config['mailtype'] = 'html';

?>