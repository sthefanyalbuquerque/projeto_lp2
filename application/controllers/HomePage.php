<?php 

    class HomePage extends MY_Controller{

        function __construct(){
            parent::__construct();
            $this->load->model('HomeModel', 'home');
        }

        public function index(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('home/index');
            $this->load->view('componente/rodape');
            $this->load->view('common/footer');
            //$this->load->library('email');
        }

        public function servicos(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('home/servicos');
            $this->load->view('common/footer');
            $this->load->view('componente/rodape');            

        }

        public function sobre(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('home/sobre');
            $this->load->view('common/footer');
            $this->load->view('componente/rodape');
        
        }

        public function contato(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('home/contato');
            $this->load->view('common/footer');
            $this->load->view('componente/rodape');
        }

        public function itens_compras(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('home/checkout');
            $this->load->view('common/footer');
            $this->load->view('componente/rodape');
        
        }

        public function login(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('home/login');
            $this->load->view('common/footer');
            $this->load->view('componente/rodape');
        
        }

        public function cadastro(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('home/cadastro');
            $this->load->view('common/footer');
            $this->load->view('componente/rodape');

        }

        public function detalhes(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('home/detalhes');
            $this->load->view('common/footer');
            $this->load->view('componente/rodape');
            
        }

    }

?>