<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('option_model', 'option');
        $this->load->helper('form');
        $this->load->library('form_validation');

    }

    public function index(){
        if($this->option->get_option('setup_executado') == 1){
            redirect('setup/alterar');
        }else{
            redirect('setup/instalar');
        }
    }

    public function instalar(){
        if($this->option->get_option('setup_executado') == 1):
            redirect('setup/alterar');
        endif;

        $dados['titulo'] = 'INFOTech - Setup do Sistema';
        $dados['h2'] = 'Setup do Sistema'; 
        $this->load->view('painel/setup', $dados);


    }

}

?>