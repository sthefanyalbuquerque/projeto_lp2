<?php

include_once APPPATH.'libraries/util/Validator.php';

    class HomeModel extends CI_Model{

        function __construct(){
            $this->load->library('util/validator');
        }

        public function lista_produtos(){
                //$produto = new Produto();
            $this->load->library('Produto');
            $v = $this->produto->get();

           //print_r($v); 

           $html = '<br><table class="table mt-5">';
           $html .= $this->table_header();
           $html .= $this->table_body($v);
           $html .= '</table>';
            return $html;
        }

        private function table_body($v){
            $html = '';
            foreach ($v as $produto){
                $html .= '<tr>';
                $html .= '<td>'.$produto['id'].'</td>';
                $html .= '<td>'.$produto['nome'].'</td>';
                $html .= '<td>'.$produto['descricao'].'</td>';
                $html .= '<td>'.$produto['preco'].'</td>';
                $html .= '<td>'.$this->action_buttons($produto['id']).'</td>';
                $html .= '</tr>';
            }
            return $html;

        }

        private function action_buttons($id){
            $html = '<a href="'.base_url('/index.php/loja/edit/'.$id).'">';
            $html .= '<i class="far fa-edit indigo-text mr-2"></i></a>';
            $html .= '<a href="'.base_url('index.php/loja/delete/'.$id).'">';
            $html .= '<i class="far fa-trash-alt red-text"></i></a>';
            return $html;
        }


        private function table_header(){
           $html = '<tr>';
           $html .='<td>#</td>';
           $html .='<td>Nome</td>';
           $html .='<td>Descrição</td>';
           $html .='<td>Preço</td>';
           $html .= '<td>Botões</td>';
           $html .= '</tr>';

           return $html;


        }

        public function salvar(){
            if(sizeof($_POST) == 0) return;

            //depois de verificar se o POST tem dados 
            //verifique a correção dos dados recebidos
            //criar as regras de validação



            if($this->validator->produto_validate()){
                $data['nome'] = $this->input->post('nome');
                $data ['descricao'] = $this->input->post('descricao');
                $data ['preco'] = $this->input->post('preco');
    
                $this->load->library('Produto');
                $this->produto->set($data);
            }
            

            
        }

        //private function validate(){
         //   $this->form_validation->set_rules('nome', 'Nome do produto', 'trim|required|min_length[4]|max_length[40]');
         //   $this->form_validation->set_rules('descricao', 'descrição do produto', 'trim|required|min_length[10]|max_length[100]');
         //   $this->form_validation->set_rules('preco', 'preço do produto', 'trim|required|decimal|greater_than[0]');
         //   return $this->form_validation->run();

        //}



        public function carregar($id){
            $this->load->library('Produto');
            $_POST = $this->produto->get_by_id($id);

        }

        public function atualizar($id){
            if(sizeof($_POST) == 0) return;

            if($this->validator->produto_validate()){
            $data['nome'] = $this->input->post('nome');
            $data ['descricao'] = $this->input->post('descricao');
            $data ['preco'] = $this->input->post('preco');

            $this->load->library('Produto');
            $status = $this->produto->update($data, $id);
            if($status) redirect('index.php/loja'); 
            
            //$status = $this->load->library('Produto');
            //if($status) redirect('index.php/loja'); 

            //return $this->produto->update($data, $id);
        }

        }


        public function delete($id){
            $this->load->library('Produto');
            $this->produto->delete($id);
        }


    }

?>